//
//  MainCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: BaseCoordinator {

  let window: UIWindow
  private let tabBarController = TabbarController()
  init(window: UIWindow) {
    self.window = window
  }

  override func start() {
    let feed = UINavigationController()
    let profile = UINavigationController()
    setupFeedFlow(feed)
    setupProfileFlow(profile)
    tabBarController.viewControllers = [feed, profile]

    window.rootViewController = tabBarController
  }

  private func setupFeedFlow(_ navController: UINavigationController) {
    navController.tabBarItem.title = "Feed"
    let feedCoordinator = FeedCoordinator(router: Router(rootController: navController))
    self.add(feedCoordinator)
    feedCoordinator.start()
  }

  private func setupProfileFlow(_ navController: UINavigationController) {
    navController.tabBarItem.title = "Profile"
    let profileCoordinator = ProfileCoordinator(router: Router(rootController: navController))
    self.add(profileCoordinator)
    profileCoordinator.start()
  }

}
