//
//  TabBarController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

final class TabbarController: UITabBarController, UITabBarControllerDelegate {

  var onFeedSelect: ((UINavigationController) -> ())?
  var onProfileSelect: ((UINavigationController) -> ())?
  var onViewDidLoad: ((UINavigationController) -> ())?

  override func viewDidLoad() {
    super.viewDidLoad()

    delegate = self
    if let controller = viewControllers?.first as? UINavigationController {
      onViewDidLoad?(controller)
    }
  }

  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    guard let controller = viewControllers?[selectedIndex] as? UINavigationController else { return }

    if selectedIndex == 0 {
      onFeedSelect?(controller)
    }
    else if selectedIndex == 1 {
      onProfileSelect?(controller)
    }
  }
}
