//
//  ProfileViewController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

  var onSettingsPressed: (() -> Void)?

  @IBOutlet weak var settingsButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    navigationController?.tabBarItem.title = "Profile"

    settingsButton.addTarget(self, action: #selector(didPressSettingsButton), for: .touchUpInside)

    // Do any additional setup after loading the view.
  }

  @objc
  func didPressSettingsButton() {
    onSettingsPressed?()
  }
}
