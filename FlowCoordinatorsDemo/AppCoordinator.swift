//
//  AppCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: BaseCoordinator {

  let window: UIWindow
  let appState: ApplicationStateType

  init(withWindow window: UIWindow, appState: ApplicationStateType) {
    self.window = window
    self.appState = appState
  }

  override func start() {
    if appState.isAuthenticated && appState.isTutorialShown {
      showMain()
    } else if appState.isAuthenticated {
      showTutorial()
    } else {
      showAuth()
    }
  }

  private func showMain() {
    let mainCoordinator = MainCoordinator(window: window)
    add(mainCoordinator)
    mainCoordinator.start()
  }

  private func showTutorial() {
    let tutorialCoordinator = TutorialCoordinator(window: window)
    unowned(unsafe) let _tutsCooridinator = tutorialCoordinator
    tutorialCoordinator.onFinish = { [weak self] in
      self?.remove(_tutsCooridinator)
      self?.showMain()
    }
    add(tutorialCoordinator)
    tutorialCoordinator.start()
  }

  private func showAuth() {
    let authCoordinator = AuthCoordinator(window: window)
    unowned(unsafe) let _authCoordinator = authCoordinator
    authCoordinator.onFinish = { [weak self] in
      self?.remove(_authCoordinator)
      self?.showTutorial()
    }
    add(authCoordinator)
    authCoordinator.start()
  }
}
