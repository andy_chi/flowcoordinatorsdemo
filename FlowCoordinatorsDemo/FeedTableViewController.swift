//
//  FeedTableViewController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {

  var onCreate: (() -> Void)?
  var onSelect: ((Int) -> Void)?


  override func viewDidLoad() {
    super.viewDidLoad()
    let createButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didPressCreateButton))
    navigationItem.rightBarButtonItem = createButton

    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
  }
  // MARK: - Table view data source

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    cell.textLabel?.text = "\(indexPath.row)"
    return cell
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    onSelect?(indexPath.row)
  }

  @objc
  func didPressCreateButton() {
    onCreate?()
  }
}
