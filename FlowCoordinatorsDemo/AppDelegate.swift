//
//  AppDelegate.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  private var appCoordinator: AppCoordinator!

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    window = UIWindow(frame: UIScreen.main.bounds)
    let state = ApplicationState(isAuth: false, isTutorial: false)
    appCoordinator = AppCoordinator(withWindow: window!, appState: state)
    appCoordinator.start()
    window?.makeKeyAndVisible()

    return true
  }

}

