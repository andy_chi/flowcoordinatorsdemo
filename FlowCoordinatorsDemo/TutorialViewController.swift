//
//  TutorialViewController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

  var onFinish: (() -> Void)?

  @IBOutlet weak var gotItButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    gotItButton.addTarget(self, action: #selector(didPressGotItButton), for: .touchUpInside)
    // Do any additional setup after loading the view.
  }

  @objc
  func didPressGotItButton() {
    onFinish?()
  }
}
