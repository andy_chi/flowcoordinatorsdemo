//
//  FeedCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class FeedCoordinator: BaseCoordinator {

  let router: RouterType
  init(router: RouterType) {
    self.router = router
  }

  override func start() {
    let feedTableViewController = FeedTableViewController(nibName: nil, bundle: nil)
    feedTableViewController.onCreate = { [weak self] in
      self?.showCreatePost()
    }
    feedTableViewController.onSelect = { [weak self] index in
      self?.showPostDetail(withIndex: index)
    }
    router.setRootModule(feedTableViewController)
  }

  private func showPostDetail(withIndex index: Int) {
    let postDetail = PostDetailViewController(nibName: nil, bundle: nil)
    _ = postDetail.view
    postDetail.textLabel.text = "\(index)"
    router.push(postDetail)
  }

  private func showCreatePost() {

  }
}
