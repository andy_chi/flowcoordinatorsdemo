//
//  LoginViewController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

  var onFinishLogin: (() -> Void)?

  @IBOutlet weak var loginButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    loginButton.addTarget(self,
                          action: #selector(didPressLoginButton),
                          for: .touchUpInside)
  }

  @objc
  func didPressLoginButton() {
    onFinishLogin?()
  }

}
