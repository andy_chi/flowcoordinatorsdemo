//
//  TutorialCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class TutorialCoordinator: BaseCoordinator {

  var onFinish: (() -> Void)?

  private var viewController: UIViewController?
  private var window: UIWindow?

  init(viewController: UIViewController) {
    self.viewController = viewController
  }

  init(window: UIWindow) {
    self.window = window
  }

  override func start() {
    let tutorialViewController = TutorialViewController(nibName: nil, bundle: nil)
    tutorialViewController.onFinish = { [weak self] in
      self?.onFinish?()
    }

    if let window = window {
      window.rootViewController = tutorialViewController
    } else {
      viewController?.present(tutorialViewController, animated: true)
    }
  }
}
