//
//  BaseCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation

class BaseCoordinator: Coordinator {

  var childCoordinators: [Coordinator] = []

  func start() { }

  // add only unique object
  func add(_ coordinator: Coordinator) {
    for element in childCoordinators {
      if element === coordinator { return }
    }
    childCoordinators.append(coordinator)
  }

  func remove(_ coordinator: Coordinator?) {
    guard
      childCoordinators.isEmpty == false,
      let coordinator = coordinator
      else { return }

    for (index, element) in childCoordinators.enumerated() {
      if element === coordinator {
        childCoordinators.remove(at: index)
        break
      }
    }
  }

  deinit {
    print("Deallocated: " + String(describing: self.self))
  }
}
