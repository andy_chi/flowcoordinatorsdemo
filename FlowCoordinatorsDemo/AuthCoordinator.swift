//
//  AuthCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class AuthCoordinator: BaseCoordinator {

  var onFinish: (() -> Void)?

  let window: UIWindow
  private weak var navigationController: UINavigationController!

  init(window: UIWindow) {
    self.window = window
  }

  override func start() {
    let onboardingVC = OnboardingViewController(nibName: nil, bundle: nil)
    onboardingVC.onFinish = { [weak self] in
      self?.onFinish?()
    }

    onboardingVC.showLogin = { [weak self] in
      self?.showLoginController()
    }

    onboardingVC.showSignup = { [weak self] in
      self?.showSignupCoordinator()
    }

    let navigation = UINavigationController(rootViewController: onboardingVC)
    navigation.isNavigationBarHidden = true
    self.navigationController = navigation
    window.rootViewController = navigation
  }


  func showLoginController() {

  }

  func showSignupCoordinator() {

  }
}

