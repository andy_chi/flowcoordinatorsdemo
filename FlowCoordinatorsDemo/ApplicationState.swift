//
//  ApplicationState.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation

protocol ApplicationStateType {
  var isAuthenticated: Bool { get }
  var isTutorialShown: Bool { get }
}

class ApplicationState: ApplicationStateType {
  let isAuthenticated: Bool
  let isTutorialShown: Bool

  init(isAuth: Bool, isTutorial: Bool) {
    isAuthenticated = isAuth
    isTutorialShown = isTutorial
  }
}
