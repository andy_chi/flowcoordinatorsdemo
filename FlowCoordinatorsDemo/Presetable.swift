//
//  Presetable.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

protocol Presentable {
  func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {

  func toPresent() -> UIViewController? {
    return self
  }
}
