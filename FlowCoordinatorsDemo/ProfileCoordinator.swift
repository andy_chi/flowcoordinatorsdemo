//
//  ProfileCoordinator.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import Foundation
import UIKit

class ProfileCoordinator: BaseCoordinator {

  let router: RouterType

  init(router: RouterType) {
    self.router = router
  }

  override func start() {
    let profileVC = ProfileViewController(nibName: nil, bundle: nil)
    profileVC.onSettingsPressed = { [weak self] in
      self?.showSettings()
    }
    router.setRootModule(profileVC)
  }

  private func showSettings() {
    //TODO: Add settings controller
  }
}
