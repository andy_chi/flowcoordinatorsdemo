//
//  OnboardingViewController.swift
//  FlowCoordinatorsDemo
//
//  Created by Andy Chikalo on 2/3/18.
//  Copyright © 2018 Andy. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

  var onFinish: (() -> Void)?
  var showLogin: (() -> Void)?
  var showSignup: (() -> Void)?

  @IBOutlet weak var skipButton: UIButton!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var signupButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()

    skipButton.addTarget(self, action: #selector(didPressButton(button:)), for: .touchUpInside)
    loginButton.addTarget(self, action: #selector(didPressButton(button:)), for: .touchUpInside)
    signupButton.addTarget(self, action: #selector(didPressButton(button:)), for: .touchUpInside)
  }

  @objc
  func didPressButton(button: UIButton) {
    if skipButton === button {
      onFinish?()
    } else if loginButton === button {
      showLogin?()
    } else if signupButton === button {
      showSignup?()
    }
  }
}
